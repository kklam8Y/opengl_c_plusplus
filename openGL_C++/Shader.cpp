#include "stdafx.h"
#include <vector>
#include <algorithm>
#include "Shader.h"

using namespace std;

string Shader::loadFile(string filepath)
{
	file.open(filepath);
	if (file.is_open()) 
	{
		string line = "";
		while (getline(file, line))
		{
			content.append(line + "\n");
		}
	}
	file.close();

	//cout << content << endl;

	return content;
}


int Shader::LoadCompile(string _path, int _type)
{
	//create handle
	int handle = glCreateShader(_type);

	//read shader file
	string shaderStr = loadFile(_path);
	const char *shaderStrPtr = shaderStr.c_str();

	//compile
	glShaderSource(handle, 1, &shaderStrPtr, NULL);
	glCompileShader(handle);

	//check shader
	GLint result = GL_FALSE;
	int logLength;

	glGetShaderiv(handle, GL_COMPILE_STATUS, &result);
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLength);
	vector<char> ShaderError((logLength > 1) ? logLength : 1);
	glGetShaderInfoLog(handle, logLength, NULL, &ShaderError[0]);
	

	if (logLength == 0)
		cout << _path + " has compiled with no errors!"  << endl;
	else
		cout << &ShaderError[0] << endl;

	return handle;

}
