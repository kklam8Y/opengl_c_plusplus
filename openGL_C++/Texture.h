#pragma once

#include "stdafx.h"

#include <string>

#include <GL\glew.h>
#include <GL\freeglut.h>
#include <GL\glut.h>

#include <fstream>
#include <iostream>

#include <IL\il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>
#include <vector>

using namespace std;

class Texture
{
public:
	Texture(GLuint, string);
	~Texture();

	void Load(string, unsigned);
	void BindTexture2Shader(int);
	void Destroy();

	void Bind();
private:
	unsigned int ID;
	GLuint texBuffer0;
	void *data;
	int height, width;
	GLuint activeTex;
	string uniform;
	GLuint uniformLoc;
};

