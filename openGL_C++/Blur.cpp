#include "stdafx.h"

#include "Blur.h"

#include <GL\glew.h>
#include <GL\freeglut.h>
#include <GL\glut.h>

Blur::Blur()
{
	GLuint projectionMatrixBlur = 0;
	GLuint modelViewBlur = 0;

}

void Blur::Init()
{

	program.Create();
	program.AddVertex("shaders/postprocess.vert");
	program.AddFragment("shaders/postprocess.frag");
	program.Attach();
	program.Link();
	program.Use();

	CreateFBO();

	projectionMatrixBlur = glGetUniformLocation(program.ID(), "projectionMatrix");
	modelViewBlur = glGetUniformLocation(program.ID(), "modelView");

	GLuint resolutionLoc = glGetUniformLocation(program.ID(), "resolution");
	GLuint blurAmountLoc = glGetUniformLocation(program.ID(), "blurAmount");
	GLuint blurSamplesLoc = glGetUniformLocation(program.ID(), "blurSamples");

	glUniform2f(resolutionLoc, 800, 600);
	glUniform1f(blurAmountLoc, 2.5f);
	glUniform1i(blurSamplesLoc, 5);

	float vertices[] = {
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 0.0f, 1.0f
	};

	// Generate the buffer name
	glGenBuffers(1, &bufQuad);
	// Bind the vertex buffer and send data
	glBindBuffer(GL_ARRAY_BUFFER, bufQuad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
}

void Blur::CreateFBO()
{

	GLuint texloc = glGetUniformLocation(program.ID(), "texture1");

	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &textureIdBlur);
	glBindTexture(GL_TEXTURE_2D, textureIdBlur);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 600, 0,
		GL_RGB, GL_UNSIGNED_BYTE, 0);


	glUniform1i(texloc, 1);

	glBindTexture(GL_TEXTURE_2D, 0);

	// create a renderbuffer object to store depth info
	glGenRenderbuffers(1, &rboIdBlur);
	glBindRenderbuffer(GL_RENDERBUFFER, rboIdBlur);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
		800, 600);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// create a framebuffer object

	glGenFramebuffers(1, &fboIdBlur);
	glBindFramebuffer(GL_FRAMEBUFFER, fboIdBlur);

	// attach the texture to FBO color attachment point
	glFramebufferTexture2D(GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
		GL_COLOR_ATTACHMENT0,  // 2. attachment point
		GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
		textureIdBlur,             // 4. tex ID
		0);                    // 5. mipmap level: 0(base)

							   // attach the renderbuffer to depth attachment point
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,      // 1. fbo target: GL_FRAMEBUFFER
		GL_DEPTH_ATTACHMENT, // 2. attachment point
		GL_RENDERBUFFER,     // 3. rbo target: GL_RENDERBUFFER
		rboIdBlur);              // 4. rbo ID

							 // check FBO status

							 // switch back to window-system-provided framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Blur::Render(int height, int width, float amount, int samples, GLuint originalTex)
{
	glUniform2f(resolutionLoc, height, width);
	glUniform1f(blurAmountLoc, amount);
	glUniform1i(blurSamplesLoc, samples);

	// setup ortographic projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);
	glGetFloatv(GL_PROJECTION_MATRIX, projMatrix);
	glUniformMatrix4fv(projectionMatrixBlur, 1, false, projMatrix);

	// clear screen and buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, originalTex);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textureIdBlur);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrix);
	glUniformMatrix4fv(modelViewBlur, 1, false, modelMatrix);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, bufQuad);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
		(void*)(3 * sizeof(float)));
	glDrawArrays(GL_QUADS, 0, 4);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(3);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Blur::StartOffScreenRendering()
{
	glBindFramebuffer(GL_FRAMEBUFFER, fboIdBlur);
}

