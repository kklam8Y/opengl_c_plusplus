#pragma once


#include <GL\glew.h>
#include <GL\freeglut.h>
#include <iostream>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

class Shader
{
private:
	ifstream file;
	string content;
	string loadFile(string);
public:
	int LoadCompile(string _path, int _type);
};