#pragma once

#include <iostream>>
#include <fstream> 
#include "Mesh.h"

using namespace std;

class Model : public Mesh
{
public:
	 Model(const char* path);
	 Mesh* mMesh;

};