#version 330

//matricies
uniform mat4 modelView;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

layout(location = 0) in vec3 Vertex;
layout(location = 1) in vec3 Color;
layout(location = 2) in vec3 Normal;
layout(location = 3) in vec2 TexCoord;

//color
uniform vec3 colorAmbient;

//to frag
out vec4 color;
out vec3 normal;
out vec2 texCoords;


vec4 worldPos;
vec4 positionRelativeToCam;


void main() {
	
	color = vec4(colorAmbient, 1.0f);
	normal = normalize(mat3(modelView) * Normal);
	
	texCoords = TexCoord;
	
   	gl_Position = projectionMatrix * viewMatrix * modelView * vec4(Vertex,1.0);
}