#version 330

vec2 gaussFilter[7];

uniform sampler2D texture0;
uniform sampler2D texture1;

out vec4 outColor;
in vec2 texCoords;

uniform vec2 resolution;
uniform float blurAmount;
uniform int blurSamples;


void main() 
{
	vec2 coords = gl_FragCoord.xy/(resolution);
	vec2 pixelSize = 5/resolution;

	int SIZE = blurSamples;
	vec3 v = vec3(0, 0, 0);
	int n = 0;
	for (int k = -SIZE; k <= SIZE; k++)
	{
		for (int j = -SIZE; j <= SIZE; j++)
		{
			v += texture(texture1, coords + vec2(k,j) * pixelSize).rgb;
			n++;
		}
	}

	vec4 colorBlur = vec4(v/n, 1);


	outColor = colorBlur * exp(1.2) + texture(texture0, coords);
}