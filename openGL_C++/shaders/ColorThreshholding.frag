#version 330

uniform sampler2D texture0;

out vec4 outColor;
in vec2 texCoords;

uniform vec2 resolution;
uniform float blurAmount;
uniform int blurSamples;


void main() 
{
	vec2 coords = gl_FragCoord.xy/resolution;
	vec4 textureColor = texture2D(texture0, coords);	
	float brightness = (textureColor.r + textureColor.g  + textureColor.b)/3.0;
//
	if(brightness > 0.5)
		outColor = textureColor	;
	else 
		outColor = vec4(0, 0, 0, 1);
}