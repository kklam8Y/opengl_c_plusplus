#version 330

//matracies
uniform mat4 viewMatrix;

//light pos
uniform vec3 lightDir;
uniform vec3 lightIntensity; //light color
uniform vec3 colorDiffuse;
uniform int lightOn;

//from vertex
in vec4 color;
in vec3 normal;
in vec2 texCoords;

uniform sampler2D texture0;

vec3 L;
float NdotL;
vec3 c;
vec4 texColor;

out vec4 outColor;

vec3 DirectionalLight()
{
	c = vec3(0,0,0);	
	
	L = normalize(mat3(viewMatrix) * lightDir);
	NdotL = dot(normal, L);
	if (NdotL > 0)
		c = vec3(colorDiffuse * lightIntensity) * NdotL;
	return c;
}


void main() {

	texColor = texture2D(texture0, texCoords.st);
	if(lightOn == 1)
	{
		outColor =  color + vec4( DirectionalLight(), 1.0f);
		outColor *= texColor;
	}
	else if(lightOn == 0)
		outColor = color + vec4(colorDiffuse, 1);
}