#version 330

//matricies
uniform mat4 modelView;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

layout(location = 0) in vec3 Vertex;
layout(location = 3) in vec2 TexCoord;

//out vec4 clipSpacePos;
void main() 
{
   	gl_Position = projectionMatrix * vec4(Vertex,1.0);	
}