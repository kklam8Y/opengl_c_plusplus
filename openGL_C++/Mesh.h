#pragma once

#include "stdafx.h"

#include <GL\glew.h>
#include <GL\freeglut.h>
#include <vector>
#include <algorithm>

using namespace std;

class Mesh
{
private:

	GLuint vertexBuffer0;
	GLuint indexBuffer0;
	GLuint normalBuffer0;
	GLuint texBuffer0;

	vector<float> verticiesBuffer;
	vector<int> indiciesBuffer;
	vector<float> normalsBuffer;
	vector<float> texBuffer;

	void genVerticeBuffer();
	void genIndiciesBuffer();
	void genNormalsBuffer();
	void genTexCoordsBuffer();

protected:
	float *verticies;
	unsigned int *indicies;
	float *normals;
	float *texCoords;

	float verticies_count;
	float normals_count;
	float indicies_count;
	float texCoords_count;
public:

	Mesh();

	void Create();
	void Draw();

	void setVerticiesArray(float*, int);
	void setIndiciesArray(unsigned int*, int);
	void setNormalsArray(float*, int);
	void setTexCoordArray(float*, int);
};