#include"stdafx.h"

#include "Program.h"
#include "Shader.h"

Program::Program()
{
	//Create Sahder Program
	
}

void Program::Create()
{
	programID = glCreateProgram();
}

void Program::AddVertex(string path)
{
	Shader shaderVert = Shader();
	vertShader = shaderVert.LoadCompile(path, GL_VERTEX_SHADER);
}

void Program::AddFragment(string path)
{
	Shader shaderFrag = Shader();
	fragShader = shaderFrag.LoadCompile(path, GL_FRAGMENT_SHADER);
}

void Program::Attach()
{
	glAttachShader(programID, vertShader);
	glAttachShader(programID, fragShader);
}

void Program::Link()
{
	glLinkProgram(programID);
	glValidateProgram(programID);
}

void Program::Use()
{
	glUseProgram(programID);
}

GLuint Program::ID()
{
	return programID;
}
