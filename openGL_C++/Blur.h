#pragma once

#include "Program.h"

class Blur 
{
private:
	GLuint resolutionLoc;
	GLuint blurAmountLoc;
	GLuint blurSamplesLoc;

	GLuint texLocBlur;
	//texture buffer
	GLuint textureIdBlur;

	//depth buffer
	GLuint rboIdBlur;

	//frame buffer
	GLuint fboIdBlur;

	GLuint projectionMatrixBlur;
	GLuint modelViewBlur;

	GLuint bufQuad;

	float modelMatrix[16];
	float viewMatrix[16];
	float projMatrix[16];

public:
	Blur();

	void Init();
	void Render(int, int, float, int, GLuint);
	void CreateFBO();
	void StartOffScreenRendering();
	Program program;
};