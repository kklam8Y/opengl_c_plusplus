#include "stdafx.h"

#include "Pyramid.h"

float p_vertices[] = { // 6 vertices in (x,y,z)
	-1.0f, -1.0f, -1.0f, // 0. left-back
	1.0f, -1.0f, -1.0f, // 1. right-back
	1.0f, -1.0f, 1.0f, // 2. right-front
	-1.0f, -1.0f, 1.0f, // 3. left-front
	0.0f, 1.0f, 0.0f, // 4. top
	0.0f, -1.0f, 0.0f // 5. bottom
};

unsigned int p_indicies[] = {

	//top pyramid
	2, 4, 3, // front face (CCW) - same as 4,3,2 or 3,2,4
	4, 2, 1, // right face  - same as 2,1,4 or 1,4,2
	0, 4, 1, // back face - same 4,1,0 or 1,0,4
	4, 0, 3, // left face - same 0,3,4 or 3,4,2
			 //bottom pyramid
			 2, 5, 3,
			 5, 2, 1,
			 0, 5, 1,
			 5, 0, 3
};

Pyramid::Pyramid()
{
	 

	Mesh::setVerticiesArray(p_vertices, sizeof(p_vertices));
	Mesh::setNormalsArray(p_vertices, sizeof(p_vertices));
	Mesh::setIndiciesArray(p_indicies, sizeof(p_indicies));
}