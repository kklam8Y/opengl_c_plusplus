#include "stdafx.h"

#include "Cube.h"


float m_vertices[] = {
	// front
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	// back
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
};

unsigned int m_indicies[] = {

	//front
	0, 1, 2,
	2, 3, 0,
	// top
	1, 5, 6,
	6, 2, 1,
	// back
	7, 6, 5,
	5, 4, 7,
	// bottom
	4, 0, 3,
	3, 7, 4,
	// left
	4, 5, 1,
	1, 0, 4,
	// right
	3, 2, 6,
	6, 7, 3
};

Cube::Cube()
{
	Mesh::setVerticiesArray(m_vertices, sizeof(m_vertices));
	Mesh::setNormalsArray(m_vertices, sizeof(m_vertices));
	Mesh::setIndiciesArray(m_indicies, sizeof(m_indicies));
}