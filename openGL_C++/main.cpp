#include "stdafx.h"

#include <Windows.h>
#include <GL\glew.h>
#include <GL\freeglut.h>
#include <GL\glut.h>
#include <iostream>
#include <string>
#include <cmath>

#include "Shader.h"
#include "Program.h"
#include "Mesh.h"
#include "Cube.h"
#include "Pyramid.h"
#include "Texture.h"
#include "Blur.h"

using namespace std;

//Methods
void init();
void changeViewPort(int, int);
void render();

//MVP Handle
GLuint modelView = 0;
GLuint projectionMatrix = 0;
GLuint viewMatrix0 = 0;

GLuint modelViewPP = 0;
GLuint projectionMatrixPP = 0;
GLuint viewMatrixPP0 = 0;

//MVP Matricies
float modelMatrix[16];
float viewMatrix[16];
float projMatrix[16];

float modelMatrixPP[16];
float viewMatrixPP[16];
float projMatrixPP[16];

float deltaX = 0;

int programID;

GLuint colorAmbient;
GLuint colorDiffuse;

Program basic = Program();
Program PostProcessing = Program();
Program Thresh = Program();

//objects
Mesh cube = Cube();
Mesh pyramid = Pyramid();

int texLoc;

GLuint bufQuad;

//textures
Texture *texBox = new Texture(GL_TEXTURE0, "texture0");
Texture *texRoof = new Texture(GL_TEXTURE0, "texture0");

GLuint FrameBuffer = 0;
GLuint renderTexture;
GLenum DrawBuffers[1];

GLuint rboId, rboId1;
GLuint textureId;
GLuint textureId1;
GLuint fboId, fboId1;

float intensity = 1;


Blur blur = Blur();

void init()
{
	glEnable(GL_DEPTH_TEST);	// depth test is necessary for most 3D scenes
	glEnable(GL_NORMALIZE);		// normalization is needed by AssImp library models
	glShadeModel(GL_FLAT);	// smooth shading mode is the default one; try GL_FLAT here!
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// this is the default one; try GL_LINE!

	 
	//Create program and attach shaders
	basic.Create();
	basic.AddVertex("shaders/basic.vert");
	basic.AddFragment("shaders/basic.frag");
	basic.Attach();
	basic.Link();
	basic.Use();

	float aspect = (float)800 /600;

	modelView = glGetUniformLocation(basic.ID(), "modelView");
	projectionMatrix = glGetUniformLocation(basic.ID(), "projectionMatrix");
	viewMatrix0 = glGetUniformLocation(basic.ID(), "viewMatrix");

	//Set projection
	glViewport(0, 0, 800, 600);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, aspect, 1.0, 1000.0);
	
	glGetFloatv(GL_PROJECTION_MATRIX, projMatrix);
	glUniformMatrix4fv(projectionMatrix, 1, false, projMatrix);


	//Object Color Uniforms
	colorAmbient = glGetUniformLocation(basic.ID(), "colorAmbient");
	colorDiffuse = glGetUniformLocation(basic.ID(), "colorDiffuse");

	glUniform3f(colorDiffuse, 1.0f, 0.0f, 1.0f);
	glUniform3f(colorAmbient, .1f, .1f, .1f);

	//Light Data
	GLuint lightDir = glGetUniformLocation(basic.ID(), "lightDir");
	GLuint lightIntensity = glGetUniformLocation(basic.ID(), "lightIntensity");
	glUniform3f(lightDir, -0.0f, -0.0f, 1.0f);

	glUniform3f(lightIntensity, intensity, intensity, intensity);

	//textures
	texBox->Load("img/p.png", GL_RGBA);
	texBox->BindTexture2Shader(programID);
	texRoof->Load("img/r.jpg", GL_RGBA);
	texRoof->BindTexture2Shader(programID);

	//objects
	cube.Create();
	pyramid.Create();
	
	blur.Init();
	blur.CreateFBO();


	//Create program and attach shaders
	PostProcessing.Create();
	PostProcessing.AddVertex("shaders/ColorThreshholding.vert");
	PostProcessing.AddFragment("shaders/ColorThreshholding.frag");
	PostProcessing.Attach();
	PostProcessing.Link();
	PostProcessing.Use();

	modelViewPP= glGetUniformLocation(PostProcessing.ID(), "modelView");
	projectionMatrixPP = glGetUniformLocation(PostProcessing.ID(), "projectionMatrix");
	viewMatrixPP0 = glGetUniformLocation(PostProcessing.ID(), "viewMatrix");


	GLuint PPtexloc1 = glGetUniformLocation(PostProcessing.ID(), "texture0");
	GLuint PPtexloc2 = glGetUniformLocation(blur.program.ID(), "texture0");
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 600, 0,
		GL_RGB, GL_UNSIGNED_BYTE, 0);

	glUniform1i(PPtexloc1, 0);
	glUniform1i(PPtexloc2, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	PostProcessing.Use();
	//create a renderbuffer object to store depth info
	glGenRenderbuffers(1, &rboId);
	glBindRenderbuffer(GL_RENDERBUFFER, rboId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
		800, 600);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// create a framebuffer object

	glGenFramebuffers(1, &fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fboId);

	// attach the texture to FBO color attachment point
	glFramebufferTexture2D(GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
		GL_COLOR_ATTACHMENT0,  // 2. attachment point
		GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
		textureId,             // 4. tex ID
		0);                    // 5. mipmap level: 0(base)

							   // attach the renderbuffer to depth attachment point
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,      // 1. fbo target: GL_FRAMEBUFFER
		GL_DEPTH_ATTACHMENT, // 2. attachment point
		GL_RENDERBUFFER,     // 3. rbo target: GL_RENDERBUFFER
		rboId);              // 4. rbo ID

							 // check FBO status

							  //switch back to window-system-provided framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	

	GLint count;

	glGetProgramiv(basic.ID(), GL_ACTIVE_UNIFORMS, &count);
	printf("Active Uniforms: %d\n", count);


	GLint size; // size of the variable
	GLenum type; // type of the variable (float, vec3 or mat4, etc)

	const GLsizei bufSize = 20; // maximum name length
	GLchar name[bufSize]; // variable name in GLSL
	GLsizei length; // name length

	for (int i = 0; i < count; i++)
	{
		glGetActiveUniform(basic.ID(), (GLuint)i, bufSize, &length, &size, &type, name);

		printf("Uniform #%d Type: %u Name: %s\n", i, type, name);
	}

	GLuint resolution = glGetUniformLocation(PostProcessing.ID(), "resolution");
	glUniform2f(resolution, 800, 600); 
	GLuint blurAmount = glGetUniformLocation(PostProcessing.ID(), "blurAmount");
	glUniform1f(blurAmount, 5);
	GLuint blurSamples = glGetUniformLocation(PostProcessing.ID(), "blurSamples");
	glUniform1i(blurSamples, 20);

	float vertices[] = {
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 0.0f, 1.0f
	};

	// Generate the buffer name
	glGenBuffers(1, &bufQuad);
	// Bind the vertex buffer and send data
	glBindBuffer(GL_ARRAY_BUFFER, bufQuad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


}

void RenderScreenTexture(GLuint texID)
{
	// setup ortographic projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);
	glGetFloatv(GL_PROJECTION_MATRIX, projMatrixPP);
	glUniformMatrix4fv(projectionMatrixPP, 1, false, projMatrixPP);

	// clear screen and buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrixPP);
	glUniformMatrix4fv(modelViewPP, 1, false, modelMatrixPP);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, bufQuad);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
		(void*)(3 * sizeof(float)));
	glDrawArrays(GL_QUADS, 0, 4);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(3);

}



void changeViewPort(int w, int h)
{
	glViewport(0, 0, w, h);
}

void Camera()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Camera view
	deltaX = deltaX + .5f;
	glRotatef(15, 1, 0, 0);
	gluLookAt(0.0, 2.0, 4.0,
		0.0, 2.0, 0.0,
		0.0, 1.0, 0.0);

	glGetFloatv(GL_MODELVIEW_MATRIX, viewMatrix);
	glUniformMatrix4fv(viewMatrix0, 1, false, viewMatrix);
}

void CubeObject()
{
	//Cube
	//;
	glPushMatrix();
	glTranslated(0, 0, 0);
	//glScaled(10, 10, 10);
	//glRotated(-deltaX, 1, 1, 1);
	glUniform3f(colorDiffuse, 1.0f, 1.0f, 1.0f);
	glUniform3f(colorAmbient, .0f, .0f, .0f);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrix);
	glUniformMatrix4fv(modelView, 1, false, modelMatrix);
	cube.Draw();
	glPopMatrix();
	//-------------------------------------------------/
}

void PyramidObject()
{
	//pyramid
	glPushMatrix();
	glRotated(deltaX, 0, 1, 0);
	glTranslated(0, 2, 0);
	glUniform3f(colorDiffuse, 1.0f, 1.0f, 1.0f);
	glUniform3f(colorAmbient, .1f, .1f, .1f);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrix);
	glUniformMatrix4fv(modelView, 1, false, modelMatrix);
	pyramid.Draw();
	glPopMatrix();
	//-------------------------------------------------/
}


void render()
{
	basic.Use();
	glClearColor(0.0f, 0.0f, 0.4f, 1.0f);

	
	GLuint lightOn = glGetUniformLocation(basic.ID(), "lightOn");
	glBindFramebuffer(GL_FRAMEBUFFER, fboId);
	//

	Camera();
	changeViewPort(800 , 600 );

	deltaX++;

	glUniform1i(lightOn, 1);
	texRoof->Bind();
	PyramidObject();

	glUniform1i(lightOn, 1);
	texBox->Bind();
	CubeObject();

	glBindTexture(GL_TEXTURE_2D, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	blur.StartOffScreenRendering();

	changeViewPort(800 , 600 );
	PostProcessing.Use();
	RenderScreenTexture(textureId);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	blur.program.Use();
	blur.Render(800, 600, 3, 5, textureId);

	glutSwapBuffers();
	glutPostRedisplay();
}


int main(int argc, char* argv[])
{
	// Initialize GLUT
	glutInit(&argc, argv);
	// Set up some memory buffers for our display
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	// Set the window size
	glutInitWindowSize(800, 600);
	// Create the window with the title "Hello,GL"
	glutCreateWindow("GL");

	cout << "Vendor: " << glGetString(GL_VENDOR) << endl;
	cout << "Renderer: " << glGetString(GL_RENDERER) << endl;
	cout << "Version: " << glGetString(GL_VERSION) << endl;

	// Bind the two functions (above) to respond when necessary
	glutReshapeFunc(changeViewPort);
	glutDisplayFunc(render);

	// Very important!  This initializes the entry points in the OpenGL driver so we can 
	// call all the functions in the API.
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		fprintf(stderr, "GLEW error");
		return 1;
	}

	init();

	glutMainLoop();
	return 0;
}