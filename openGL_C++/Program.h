#pragma once

#include "stdafx.h"

#include <fstream>
#include <iostream>
#include <string>

#include <GL\glew.h>
#include <GL\freeglut.h>
#include <GL\glut.h>

using namespace std;

class Program
{
private:
	GLuint programID;

	GLuint vertShader;
	GLuint fragShader;
public:
	Program();

	void Create();

	void AddVertex(string);
	void AddFragment(string);

	void Attach();
	void Link();
	
	void Use();

	GLuint ID();
};