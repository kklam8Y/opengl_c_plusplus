#include "stdafx.h"

#include "Texture.h"
#undef _UNICODE

Texture::Texture(GLuint texBufferNo, string uniformName)
{
	activeTex = texBufferNo;
	uniform = uniformName;

	

	ilInit();
	iluInit();
	ilutRenderer(ILUT_OPENGL);
}

void Texture::Load(string filepath, unsigned format)
{
	//Destroy();

	ilGenImages(1, &ID);
	ilEnable(IL_ORIGIN_SET);
	ilOriginFunc(IL_ORIGIN_LOWER_LEFT);

	ilBindImage(ID);

	wstring file(filepath.begin(), filepath.end()); //http://stackoverflow.com/questions/37074574/c-devil-function-illoadimage-program-exit-access-violation

	if (ilLoadImage((ILstring)file.c_str())) //http://stackoverflow.com/questions/5734881/loading-an-image-using-devil-and-opengl-in-c
	{
		ilConvertImage(format, IL_UNSIGNED_BYTE);
		cout << "load success: " + filepath << endl;
	}
	else
		cout << "load failed: " + filepath << endl;
}

void Texture::BindTexture2Shader(int programID)
{
	glEnable(GL_TEXTURE_2D);

	//generate a texture handle or unique ID for this texture
	glActiveTexture(activeTex);
	glGenTextures(1, &texBuffer0);

	//bind the texture
	glBindTexture(GL_TEXTURE_2D, texBuffer0);

	//use an alignment of 1 to be safe
	//this tells OpenGL how to unpack the RGBA bytes we will specify
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//set up our texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	width = ilGetInteger(IL_IMAGE_WIDTH);
	height = ilGetInteger(IL_IMAGE_HEIGHT);
	data = ilGetData();

	//upload our ByteBuffer to GL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	
	uniformLoc = glGetUniformLocation(programID, uniform.c_str());
	glUniform1i(uniformLoc, activeTex);

	glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture()
{
}

void Texture::Destroy()
{
	if (ID)
		ilDeleteImages(1, &ID);
}

void Texture::Bind()
{
	glActiveTexture(activeTex);
	glBindTexture(GL_TEXTURE_2D, texBuffer0);
}
