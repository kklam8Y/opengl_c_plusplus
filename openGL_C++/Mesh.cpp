#include "stdafx.h"
#include "Mesh.h"

Mesh::Mesh()
{

}

float m_texCoord[] = {
	0.0, 0.0,  //bottom left of texture
    1.0, 0.0,  //bottom right "    "
    1.0, 1.0,  //top right    "    "
	0.0, 1.0,
	0.0, 1.0,
	1.0, 1.0,
	1.0, 0.0,
	0.0, 0.0,

	0.0, 0.0,  //bottom left of texture
	1.0, 0.0,  //bottom right "    "
	1.0, 1.0,  //top right    "    "
	0.0, 1.0,
	0.0, 1.0,
	1.0, 1.0,
	1.0, 0.0,
	0.0, 0.0,

	0.0, 0.0,  //bottom left of texture
	1.0, 0.0,  //bottom right "    "
	1.0, 1.0,  //top right    "    "
	0.0, 1.0,
	0.0, 1.0,
	1.0, 1.0,
	1.0, 0.0,
	0.0, 0.0,
};

void Mesh::genVerticeBuffer()
{

	glGenBuffers(1, &vertexBuffer0); 

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer0);

	for (int i = 0; i < verticies_count; i++)
	{
		verticiesBuffer.push_back(*(verticies + i));
	}

	glBufferData(GL_ARRAY_BUFFER, verticies_count, &verticiesBuffer.front(), GL_STATIC_DRAW);

}

void Mesh::genNormalsBuffer()
{

	glGenBuffers(1, &normalBuffer0);

	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer0);

	for (int i = 0; i < normals_count; i++)
	{
		normalsBuffer.push_back(*(verticies + i));
	}

	glBufferData(GL_ARRAY_BUFFER, normals_count, &normalsBuffer.front(), GL_STATIC_DRAW);

}

void Mesh::genIndiciesBuffer()
{

	glGenBuffers(1, &indexBuffer0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer0);

	for (int i = 0; i < indicies_count; i++)
	{
		indiciesBuffer.push_back(*(indicies + i));
	}

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies_count, &indiciesBuffer.front(), GL_STATIC_DRAW);

}

void Mesh::genTexCoordsBuffer()
{
	glGenBuffers(1, &texBuffer0);

	setTexCoordArray(m_texCoord, sizeof(m_texCoord));

	int i, n;

	//for (n = n = 0; n < 6; n++)
	{
		//i = 0;

		for (i = 0; i < texCoords_count; i++)
		{
			texBuffer.push_back(*(texCoords + i));
		}
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, texBuffer0);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(m_texCoord), &m_texCoord, GL_STATIC_DRAW);
}

void Mesh::Create()
{
	genVerticeBuffer();
	genIndiciesBuffer();
	genNormalsBuffer();
	genTexCoordsBuffer();
}

void Mesh::Draw()
{
	glFrontFace(GL_CW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer0);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		false,           // normalized?
		0,                  // stride
		/*(void*)*/0            // array buffer offse
	);

	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer0);
	glVertexAttribPointer(
		2,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		false,           // normalized?
		0,                  // stride
		/*(void*)*/0            // array buffer offse
	);

	glBindBuffer(GL_ARRAY_BUFFER, texBuffer0);
	glVertexAttribPointer(
		3,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_FLOAT,           // type
		false,           // normalized?
		0,                  // stride
		/*(void*)*/0            // array buffer offse
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer0);
	// Draw the triangle !
	//glDrawArrays(GL_TRIANGLES, 0, verticiesBuffer.capacity()); // Starting from vertex 0; 3 vertices total -> 1 triangle
	glDrawElements(GL_TRIANGLES, indicies_count, GL_UNSIGNED_INT, 0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
}

void Mesh::setVerticiesArray(float* array_prt, int size)
{
	verticies = array_prt;
	verticies_count = size;
}

void Mesh::setIndiciesArray(unsigned int* array_prt, int size)
{
	indicies = array_prt;
	indicies_count = size;
}

void Mesh::setNormalsArray(float* array_prt, int size)
{
	normals = array_prt;
	normals_count = size;
}

void Mesh::setTexCoordArray(float* array_prt, int size)
{
	texCoords = array_prt;
	texCoords_count = size;
}